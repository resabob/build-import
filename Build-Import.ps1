﻿# DRAFT - work in progress
# Save this script in the same folder as the two excel files, then run the script. 
# Make sure the headers listed below match the headers in the excel files.
# Lastly, make sure your computer is capable of running local scripts
# Set-ExecutionPolicy remoteSigned 

Param 
(
)

begin
{
	# The location where the output file will be saved.
	$ResultsFile = (Join-Path $PSScriptRoot -ChildPath "results.csv")

	# Detects the location of the powershell script. Should contain the two excel files.
	$Path = $PSScriptRoot 
	
	# Header 1 - used to compare values in both excel files. i.e. "UIC"
	$header1 = "UIC"
	
	# Header 2 - value contianing the score or whatever value to be saved in the import file. i.e. "SCORE"
	$header2 = "SCORE"
	
	# Header 3 - the second value to be saved to the import file. i.e. "OTHER ID"
	$header3 = "Other ID"
}

process
{
	# Find the files iwth .xlsx extensions
	$Files = Get-ChildItem -Path $Path -Filter *.xlsx

	# This will be used to store the results prior to saving csv file.
	$Results = @()

	# Function to turn excel files to CSV files
	Function ConvertXtoC ($FileName, $CSVPath)
	{
	    $excelFile = $FileName.Fullname
	    $E = New-Object -ComObject Excel.Application
	    $E.Visible = $false
	    $E.DisplayAlerts = $false
	    $wb = $E.Workbooks.Open($excelFile)
	    foreach ($ws in $wb.Worksheets)
	    {
	        $n = $FileName.BaseName + "_" + $ws.Name
			$nPath = (Join-Path $CSVPath -ChildPath ("$($n).csv"))
	        $ws.SaveAs($nPath, 6)
	    }
	    $E.Quit()
	}

	# Function to check the header name of a specific column in a CSV file #
	Function Check-Header($FileName,$Header,$Position)
	{
		if((((Get-Content $FileName)[0] -split(','))[$Position]) -eq $Header)
		{
			return $True
		}
	}

	#Function to find the UIC value in the cross reference file.
	Function Find-ID($UIC)
	{
		$XFile | Where {$_.($header1) -eq $UIC} | Select-Object -ExpandProperty $header3
	}

	##### Start Processing #####
	
	# Loops through each excel file and sends path to ConvertXtoC function.
	foreach($File in $Files)
	{
		Write-Output "Converting file to CSV $($file.Fullname)"
	    ConvertXtoC -FileName $File -CSVPath $Path
	}

	$CSVFiles = Get-ChildItem -Path $Path -Filter *.csv

	foreach ($file in $CSVFiles)
	{
		# Checks the first and second header
		if((Check-Header $file.FullName $header1 0) -and (Check-Header $file.FullName $header2 1) )
		{
			Write-Output "Export File Found: $($file.fullname)"
			$EFile = Import-Csv ($file.fullname)
			try {Remove-Item -Path ($file.fullname)}
			catch {Write-Output "Failed to clean up CSV file $($file.fullname)"}
		}
		
		# Checks the header in column 2 
		elseif((Check-Header $file.FullName $header3 1))
		{
			Write-Output "Reference File Found: $($file.fullname)"
			$XFile = Import-Csv ($file.fullname)
			try {Remove-Item -Path ($file.fullname)}
			catch {Write-Output "Failed to clean up CSV file $($file.fullname)"}
		}	
	}

	# Loops through each record in the "Export" file.
	Foreach ($record in $EFile)
	{
		# Sends UIC to the Find-ID function to see if a match is found in cross reference file. 
		$ID = Find-ID $record.($header1)
		
		# If a match is found, save results to a custom powershell object
		if ($ID)
		{ 
			Write-output "Found a match for $header1 $($record.UIC)" 
			$Object = New-Object PSCustomObject 
			$Object | Add-Member Noteproperty $header1 ($record.($header1))
			$Object | Add-Member Noteproperty $header2 ($record.($header2))
			$Object | Add-Member Noteproperty $header3 $ID
			
			# Populate array with each of the objects
			$results += $Object
		}
		
		# Clears the variable to prevent reuse
		$OtherID = $Null
	}
	
	# Exports the values requested to the results file (aka "import" file). 
	$results | Select-Object $header3, $header2 | Export-Csv $ResultsFile -NoType 
	
	#Open Excel
	Start-Sleep -Seconds 2
	if ($Object) {Invoke-Item $ResultsFile}
	else {Write-Output "Failed to compile results, check to make sure the excel files exist with the correct headers."}
}
